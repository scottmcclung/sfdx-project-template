# SFDX Manifest Project Scaffold

This repo is designed to shortcut the project set up time for the most commonly used project config.

Includes:

1. Jest
1. Sa11y
1. Package manifest
1. Tailwind CSS
1. Laravel Mix
1. Prettier
1. Bitbucket pipeline example
1. CI/CD tools

## Installation

Run `yarn` to install the following

-   Jest
-   Sa11y
-   Tailwind CSS
-   Laravel Mix
-   Prettier

### Jest

Jest is the official testing framework supported by Salesforce for LWC.

The Jest configuration for Salesforce is already in place. If the `Invalid sourceApiVersion found in sfdx-project.json.` error
message is received, it generally means that the sfdx-lwc-jest package needs a rev bump.

Run `yarn test:unit` to execute all tests.

Run `yarn test:unit:watch` to execute tests in watch mode.

Also includes a launch.json config file for VSCode so LWC tests can be run in the debugger.

### Sa11y

Sa11y is a set of Salesforce Accessibility Automation Libraries [https://github.com/salesforce/sa11y](https://github.com/salesforce/sa11y)

However, they are not specific to Salesforce and can be used to test any UI supported by axe-core for accessibility. These libraries are designed to be flexible, customizable and reusable to support automated accessibility testing in different testing workflows from unit to integration tests.

The Jest integration configured in this repo provides a `toBeAccessible()` accessibility matcher for Jest tests.

-   integrates the assertAccessible API with the Jest assertion API

```js
it("is accessible", () => {
    const element = createElement("c-hello-conditional-rendering", {
        is: HelloConditionalRendering,
    });

    element.areDetailsVisible = true;
    element.areMoreDetailsVisible = true;
    document.body.appendChild(element);

    return Promise.resolve().then(() => expect(element).toBeAccessible());
});
```

### Tailwind

TailwindCss provides low level utility CSS classes that encapsulates a design system and allows you to apply styles without writing CSS.

The Tailwind configuration is set up to generate the full suite of responsive utilities with a "t-" prefix. It leaves out most of the default color palatte to reduce the file size and disables the normalizing preflight settings since I've noticed that these have some minor conflicts with the Lightning Design System.

If a different prefix is needed, this can be changed in the `tailwind.config.js` file.

PurgeCSS is configured to use LWC template files as the source for minimizing the classes that are included in the final minified production stylesheet. The targets can be adjusted in tailwind.config.js.

The base css file can be found at `resources/css/main.css`.

Running `yarn dev` will compile this to `force-app/main/default/staticresources/tailwind.css`.

Running `yarn production` will compile and minify the `tailwind.css` file.

A default non-minified version of tailwind.css is pre-compiled into static resources and can be deployed as is. This avoids the need to generate the xml file and supports css autocompletion tools.

Since LWC has a file size limit of 131k characters, we aren't able to push the full Tailwind css file up as an LWC style component.
Instead, store it as a static resource and use the resource loader to pull it into a component.

```js
import tailwindcss from '@salesforce/resourceUrl/tailwind.css';
import { loadStyle } from 'lightning/platformResourceLoader';

connectedCallback() {
    loadStyle(this, tailwindcss);
}
```

### Laravel Mix

Laravel Mix is a wrapper around webpack that simplifies the set up and use for 80% of the use cases. It installs and manages the Webpack dependency so we don't have to.

By default it is configured to compile Tailwind CSS but additional js compilation is easy to activate. Sample configurations for this can be found in the `webpack.mix.js` config file.

Run `yarn dev` to compile the project for development
Run `yarn production` to compile for production

### Prettier

Prettier is an opinionated code formatter. The .prettierc is configured with my opinion. :)

### Bitbucket Pipeline

A example pipeline yml file is included.

### CI/CD Script Tools

Script tools are located in the `/scripts/shell` directory.
These will assist in setting up the CI/CD pipeline.

#### Scratch Org Limits

If using development orgs as dev hubs for CI/CD you'll quickly run into the limit of daily scratch orgs even if you're deleting
them after every run. To get around this limit, the installation script \(`/scripts/install-utils.sh`\) installs the `sfdx-devhub-pool` sfdx-cli plugin
to allow us to use more than one dev org as a dev hub.

## Salesforce Notes

### Org Development Model

The org development model allows you to connect directly to a non-source-tracked org (sandbox, Developer Edition (DE) org, Trailhead Playground, or even a production org) to retrieve and deploy code directly. This model is similar to the type of development you have done in the past using tools such as Force.com IDE or MavensMate.

To start developing with this model in Visual Studio Code, see [Org Development Model with VS Code](https://forcedotcom.github.io/salesforcedx-vscode/articles/user-guide/org-development-model). For details about the model, see the [Org Development Model](https://trailhead.salesforce.com/content/learn/modules/org-development-model) Trailhead module.

If you are developing against non-source-tracked orgs, use the command `SFDX: Create Project with Manifest` (VS Code) or `sfdx force:project:create --manifest` (Salesforce CLI) to create your project. If you used another command, you might want to start over with this command to create a Salesforce DX project.

When working with non-source-tracked orgs, use the commands `SFDX: Deploy Source to Org` (VS Code) or `sfdx force:source:deploy` (Salesforce CLI) and `SFDX: Retrieve Source from Org` (VS Code) or `sfdx force:source:retrieve` (Salesforce CLI). The `Push` and `Pull` commands work only on orgs with source tracking (scratch orgs).

## The `sfdx-project.json` File

The `sfdx-project.json` file contains useful configuration information for your project. See [Salesforce DX Project Configuration](https://developer.salesforce.com/docs/atlas.en-us.sfdx_dev.meta/sfdx_dev/sfdx_dev_ws_config.htm) in the _Salesforce DX Developer Guide_ for details about this file.

The most important parts of this file for getting started are the `sfdcLoginUrl` and `packageDirectories` properties.

The `sfdcLoginUrl` specifies the default login URL to use when authorizing an org.

The `packageDirectories` filepath tells VS Code and Salesforce CLI where the metadata files for your project are stored. You need at least one package directory set in your file. The default setting is shown below. If you set the value of the `packageDirectories` property called `path` to `force-app`, by default your metadata goes in the `force-app` directory. If you want to change that directory to something like `src`, simply change the `path` value and make sure the directory you’re pointing to exists.

```json
"packageDirectories" : [
    {
      "path": "force-app",
      "default": true
    }
]
```

## Part 2: Working with Source

For details about developing against scratch orgs, see the [Package Development Model](https://trailhead.salesforce.com/en/content/learn/modules/sfdx_dev_model) module on Trailhead or [Package Development Model with VS Code](https://forcedotcom.github.io/salesforcedx-vscode/articles/user-guide/package-development-model).

For details about developing against orgs that don’t have source tracking, see the [Org Development Model](https://trailhead.salesforce.com/content/learn/modules/org-development-model) module on Trailhead or [Org Development Model with VS Code](https://forcedotcom.github.io/salesforcedx-vscode/articles/user-guide/org-development-model).

## Part 3: Deploying to Production

Don’t deploy your code to production directly from Visual Studio Code. The deploy and retrieve commands do not support transactional operations, which means that a deployment can fail in a partial state. Also, the deploy and retrieve commands don’t run the tests needed for production deployments. The push and pull commands are disabled for orgs that don’t have source tracking, including production orgs.

Deploy your changes to production using [packaging](https://developer.salesforce.com/docs/atlas.en-us.sfdx_dev.meta/sfdx_dev/sfdx_dev_dev2gp.htm) or by [converting your source](https://developer.salesforce.com/docs/atlas.en-us.sfdx_cli_reference.meta/sfdx_cli_reference/cli_reference_force_source.htm#cli_reference_convert) into metadata format and using the [metadata deploy command](https://developer.salesforce.com/docs/atlas.en-us.sfdx_cli_reference.meta/sfdx_cli_reference/cli_reference_force_mdapi.htm#cli_reference_deploy).
