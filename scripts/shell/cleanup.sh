#!/bin/bash
# Usage: sh scripts/shell/cleanup.sh

# Import defaults
. ./scripts/shell/lib/defaults.sh

# Import functions
. ./scripts/shell/lib/sfdx.sh

# Main script starts here
SCRATCH_ORG_NAME="${1:-validation}"
echo "*** Deleting $SCRATCH_ORG_NAME"
delete_org $SCRATCH_ORG_NAME
