#!/bin/bash
set -e

# Usage: sh scripts/shell/validate.sh

# Import defaults
. ./scripts/shell/lib/defaults.sh

# Import functions
. ./scripts/shell/lib/sfdx.sh

# Main script starts here
SCRATCH_ORG_NAME="${1:-validation}"
echo "*** Validating $BITBUCKET_BRANCH to $SCRATCH_ORG_NAME"
create_org_from_pool $SCRATCH_ORG_NAME 1 $SCRATCH_DEF_PATH $DEVHUB_POOL
source_path_deploy $SCRATCH_ORG_NAME "force-app"
assign_permset $SCRATCH_ORG_NAME $PERMSET_NAME
run_jest_tests
run_local_tests $SCRATCH_ORG_NAME
delete_org $SCRATCH_ORG_NAME
