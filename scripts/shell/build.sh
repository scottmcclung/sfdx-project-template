# Exit if any command fails
set -e

# Get default config values from scratch config file
source .buildconfig

# Create a scratch org from a pool of authorized dev hubs.
echo "Creating a new scratch org..."
sfdx force:org:create -f config/project-scratch-def.json -d "${SCRATCH_DEFAULT_DURATION:-1}" --setdefaultusername -p "${DEV_HUBS:-hub}" -a "${SCRATCH_DEFAULT_ALIAS:-next-scratch}"

# Requires the devhubpool plugin to be installed
# sfdx plugins:install sfdx-devhub-pool
# sfdx devhubpool:org:create -f config/project-scratch-def.json -d "${SCRATCH_DEFAULT_DURATION:-1}" --setdefaultusername -p "${DEV_HUBS:-hub}" -a "${SCRATCH_DEFAULT_ALIAS:-next-scratch}"


# Surgically deploy the project to the scratch org if needed.  Reference paths in the dependency order.
# echo "Deploying..."
# sfdx force:source:deploy -p "insert path to dependencies here"
# echo "Deploying..."
# sfdx force:source:deploy -p "insert path to app code here, path2"
# echo "Deploying permission set..."
# sfdx force:source:deploy -p "path to permission set"

# Push the repo into the org.
# Do this even if using the path specific deploy method above so the first save
# doesn't have to resync everything while the developer waits.
sfdx force:source:push -f

# Assign the perm set to the scratch org user
echo "Assigning permissions..."
sfdx force:user:permset:assign -n "insert name of perm set"

# Display the scratch org info and access url
echo "Scratch org details:"
sfdx force:org:display --verbose
sfdx force:org:open -r

# Run a seeder
# sfdx force:apex:execute -f scripts/apex/seed-scratch.apex

# Open the scratch org in the browser
echo "Opening the scratch org in the browser..."
sfdx force:org:open
