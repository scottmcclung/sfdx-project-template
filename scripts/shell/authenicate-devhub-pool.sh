#!/bin/bash
# Usage: sh scripts/url-login.sh

# Import functions
. ./scripts/shell/lib/sfdx.sh

url_login $HUB_AUTH_URL1 $HUB_ALIAS1
url_login $HUB_AUTH_URL2 $HUB_ALIAS2
url_login $HUB_AUTH_URL3 $HUB_ALIAS3
url_login $HUB_AUTH_URL4 $HUB_ALIAS4
url_login $HUB_AUTH_URL5 $HUB_ALIAS5
url_login $HUB_AUTH_URL6 $HUB_ALIAS6
url_login $HUB_AUTH_URL7 $HUB_ALIAS7
url_login $HUB_AUTH_URL8 $HUB_ALIAS8
url_login $HUB_AUTH_URL9 $HUB_ALIAS9
url_login $HUB_AUTH_URL10 $HUB_ALIAS10
get_info
