echo "*** Report Prepackaged with the latest Salesforce Full Image"
echo "*** node Version"
node --version
echo "*** npm Version"
npm --version

echo "*** Install yarn"
curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list
apt update && apt install -y yarn

echo "*** Report SFDX Info"
sfdx --version

echo "*** Report existing plugins"
sfdx plugins --core

echo "*** Install custom plugins"
#echo "y" | sfdx plugins:install shane-sfdx-plugins
#echo "y" | sfdx plugins:install sfpowerkit
echo 'y' | sfdx plugins:install sfdx-essentials
echo "y" | sfdx plugins:install sfdx-devhub-pool

echo "*** Report all installed plugins"
sfdx plugins

echo "*** Install node modules"
yarn install