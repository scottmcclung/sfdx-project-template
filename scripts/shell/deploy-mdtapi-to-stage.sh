#!/bin/bash
# Usage: sh scripts/deploy-mdtapi.sh

# Import functions
. ./scripts/shell/lib/sfdx.sh
. ./scripts/shell/lib/utilities.sh

TARGET_ORG_ALIAS=${1:-deploy-temp}

OUTPUT_ROOT=${2:-pre-mdtapi}
PKG_NAME=${3:-unpackaged}
SRC_ROOT=${4:-force-app/main}

## Deploys any dependencies on non Account Planning objects. (e.g. Contact)
## Uses the essentials plugin because sfdx by default converts/deploys everything that is in an object
## directory even if the package.xml is very specific.  This plugin creates a deployment package that is filtered
## to ONLY what is specified in the package file.
sfdx force:source:convert -d $OUTPUT_ROOT -x manifest/staging-pre-deploy-package.xml -n $PKG_NAME
mkdir $OUTPUT_ROOT-filtered
sfdx essentials:metadata:filter-from-packagexml -p manifest/staging-pre-deploy-package.xml -i $OUTPUT_ROOT -o $OUTPUT_ROOT-filtered/$PKG_NAME
## Deploy from the filtered results
deploy_mdtapi $TARGET_ORG_ALIAS $OUTPUT_ROOT-filtered $PKG_NAME
