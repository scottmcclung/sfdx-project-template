#!/bin/bash
# Usage: sh scripts/jwt-login.sh

# Import functions
. ./scripts/shell/lib/sfdx.sh

jwt_login $1 $2 $3 $4 $5
get_info
