const { colors } = require("tailwindcss/defaultTheme");

module.exports = {
    prefix: "t-",
    theme: {
        extend: {
            spacing: {
                "72": "18rem",
                "80": "20rem",
            },
        },
        colors: {
            black: colors.black,
            white: colors.white,
            gray: colors.gray,
        },
    },
    variants: {},
    plugins: [],
    corePlugins: {
        preflight: false,
    },
    purge: ["./force-app/main/default/**/*.html"],
};

// prefixed by default.
// preflight deactived in favor of slds preflight to avoid some minor conflicts.
// color palette limited to black, white, and gray in favor of slds palette and to manage down the final size.
